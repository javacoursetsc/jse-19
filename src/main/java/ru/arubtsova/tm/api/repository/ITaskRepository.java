package ru.arubtsova.tm.api.repository;

import ru.arubtsova.tm.api.IRepository;
import ru.arubtsova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

    List<Task> findAll(Comparator<Task> comparator);

    void removeAllByProjectId(String projectId);

    Task bindTaskToProject(String taskId, String projectId);

    Task unbindTaskFromProject(String projectId);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
