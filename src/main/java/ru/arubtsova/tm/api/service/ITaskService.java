package ru.arubtsova.tm.api.service;

import ru.arubtsova.tm.api.IService;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String name, String description);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

    Task updateTaskByIndex(Integer index, String name, String description);

    Task updateTaskById(String id, String name, String description);

    Task startTaskByIndex(Integer index);

    Task startTaskById(String id);

    Task startTaskByName(String name);

    Task finishTaskByIndex(Integer index);

    Task finishTaskById(String id);

    Task finishTaskByName(String name);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByName(String name, Status status);

}
