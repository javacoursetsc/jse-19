package ru.arubtsova.tm.api.service;

import ru.arubtsova.tm.api.IService;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAll(Comparator<Project> comparator);

    Project add(String name, String description);

    Project findOneByIndex(Integer index);

    Project findOneByName(String name);

    Project removeOneByIndex(Integer index);

    Project removeOneByName(String name);

    Project updateProjectByIndex(Integer index, String name, String description);

    Project updateProjectById(String id, String name, String description);

    Project startProjectByIndex(Integer index);

    Project startProjectById(String id);

    Project startProjectByName(String name);

    Project finishProjectByIndex(Integer index);

    Project finishProjectById(String id);

    Project finishProjectByName(String name);

    Project changeProjectStatusByIndex(Integer index, Status status);

    Project changeProjectStatusById(String id, Status status);

    Project changeProjectStatusByName(String name, Status status);

}
