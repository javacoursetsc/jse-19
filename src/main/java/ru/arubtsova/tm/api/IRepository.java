package ru.arubtsova.tm.api;

import ru.arubtsova.tm.model.AbstractEntity;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    List<E> findAll();

    E add(E entity);

    E findById(String id);

    void clear();

    E removeById(String id);

    void remove(E entity);

}
