package ru.arubtsova.tm.service;

import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.api.service.ITaskService;
import ru.arubtsova.tm.enumerated.Status;
import ru.arubtsova.tm.exception.empty.EmptyIdException;
import ru.arubtsova.tm.exception.empty.EmptyNameException;
import ru.arubtsova.tm.exception.system.IndexIncorrectException;
import ru.arubtsova.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll(final Comparator<Task> comparator) {
        if (comparator == null) return null;
        return taskRepository.findAll(comparator);
    }

    @Override
    public Task add(final String name, final String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) return null;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
        return task;
    }

    @Override
    public Task findOneByIndex(Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task findOneByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.findOneByName(name);
    }

    @Override
    public Task removeOneByIndex(Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        return taskRepository.removeOneByIndex(index);
    }

    @Override
    public Task removeOneByName(final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return taskRepository.removeOneByName(name);
    }

    @Override
    public Task updateTaskByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findById(id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskByIndex(Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskByIndex(Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task finishTaskByName(String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETE);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Task task = findById(id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = findOneByName(name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

}
