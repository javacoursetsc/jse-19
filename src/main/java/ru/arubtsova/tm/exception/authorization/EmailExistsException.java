package ru.arubtsova.tm.exception.authorization;

import ru.arubtsova.tm.exception.AbstractException;

public class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Another User Is Already Using The Same Email, Please Try Another One...");
    }

}
