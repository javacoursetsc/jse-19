package ru.arubtsova.tm.command.project;

import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

public class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "delete a project by id.";
    }

    @Override
    public void execute() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Id:");
        final String id = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeById(id);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully removed");
    }

}
