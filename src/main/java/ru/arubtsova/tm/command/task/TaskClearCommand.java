package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String description() {
        return "delete all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("Task Clear:");
        serviceLocator.getTaskService().clear();
    }

}
