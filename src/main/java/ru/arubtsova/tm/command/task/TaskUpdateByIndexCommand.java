package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-index";
    }

    @Override
    public String description() {
        return "update a task by index.";
    }

    @Override
    public void execute() {
        System.out.println("Task:");
        System.out.println("Enter Task Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter New Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = serviceLocator.getTaskService().updateTaskByIndex(index, name, description);
        if (taskUpdate == null) throw new TaskNotFoundException();
        System.out.println("Task was successfully updated");
    }

}
