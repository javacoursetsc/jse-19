package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String description() {
        return "delete a task by index.";
    }

    @Override
    public void execute() {
        System.out.println("Task Removal:");
        System.out.println("Enter Task Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().removeOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Task was successfully removed");
    }

}
