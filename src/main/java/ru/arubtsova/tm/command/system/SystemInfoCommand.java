package ru.arubtsova.tm.command.system;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.util.NumberUtil;

public class SystemInfoCommand extends AbstractCommand {

    @Override
    public String arg() {
        return "-i";
    }

    @Override
    public String name() {
        return "info";
    }

    @Override
    public String description() {
        return "show system information.";
    }

    @Override
    public void execute() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.format(maxMemory);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("System Information:");
        System.out.println("Available processors: " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.format(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + NumberUtil.format(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.format(usedMemory));
    }

}