package ru.arubtsova.tm.command.authorization;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserLogInCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "login";
    }

    @Override
    public String description() {
        return "authorization.";
    }

    @Override
    public void execute() {
        System.out.println("Login:");
        System.out.println("Enter Login:");
        final String login = TerminalUtil.nextLine();
        System.out.println("Enter Password:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
        System.out.println("You have been successfully authorised");
    }

}
