package ru.arubtsova.tm.repository;

import ru.arubtsova.tm.api.repository.IProjectRepository;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.AbstractEntity;
import ru.arubtsova.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(Comparator<Project> comparator) {
        final List<Project> projects = new ArrayList<>(entities);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public Project findOneByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Project findOneByName(final String name) {
        for (final Project project : entities) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final Integer index) {
        final Project project = findOneByIndex(index);
        if (project == null) throw new ProjectNotFoundException();
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String name) {
        final Project project = findOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
        entities.remove(project);
        return project;
    }

}
