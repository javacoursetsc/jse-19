package ru.arubtsova.tm.repository;

import ru.arubtsova.tm.api.repository.ITaskRepository;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> tasks = new ArrayList<>(entities);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public List<Task> findAllByProjectId(final String projectId) {
        final List<Task> taskList = new ArrayList<>();
        for (final Task task : entities) if (projectId.equals(task.getProjectId())) taskList.add(task);
        return taskList;
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        entities.removeIf(task -> projectId.equals(task.getProjectId()));
    }

    @Override
    public Task bindTaskToProject(final String taskId, final String projectId) {
        final Task task = findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String taskId) {
        final Task task = findById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
        return task;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return entities.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : entities) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) throw new TaskNotFoundException();
        entities.remove(task);
        return task;
    }

    @Override
    public Task removeOneByName(final String name) {
        final Task task = findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        entities.remove(task);
        return task;
    }

}
